package main.java.spells.healing;

import main.java.basestats.SpellModifiers;
import main.java.spells.abstractions.HealingSpell;

public class Regrowth implements HealingSpell {

    @Override
    public double getHealingAmount() {
        return SpellModifiers.REGROWTH_HEAL;
    }

    @Override
    public String getSpellName() {
        return "Regrowth";
    }
}
