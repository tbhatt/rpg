package main.java.spells.abstractions;

public enum SpellType {
    ArcaneMissile,
    Barrier,
    ChaosBolt,
    Rapture,
    Regrowth,
    SwiftMend
}
