package main.java.items.rarity;

import main.java.consolehelpers.Color;
import main.java.items.rarity.abstractions.Rarity;

public class Legendary implements Rarity {
    // Stat modifier
    private double powerModifier = 2;
    // Color for display purposes
    private String itemRarityColor = Color.YELLOW;

    // Public properties
    public double powerModifier() {
        return powerModifier;
    }

    public String getItemRarityColor() {
        return itemRarityColor;
    }
}
