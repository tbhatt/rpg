package main.java.items.rarity;

import main.java.consolehelpers.Color;
import main.java.items.rarity.abstractions.Rarity;

public class Rare implements Rarity {
    // Stat modifier
    private double powerModifier = 1.4;
    // Color for display purposes
    private String itemRarityColor = Color.BLUE;

    // Public properties
    public double powerModifier() {
        return powerModifier;
    }

    public String getItemRarityColor() {
        return itemRarityColor;
    }
}
