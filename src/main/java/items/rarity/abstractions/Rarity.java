package main.java.items.rarity.abstractions;

public interface Rarity {
    double powerModifier();
    String getItemRarityColor();
}
