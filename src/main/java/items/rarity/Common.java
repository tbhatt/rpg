package main.java.items.rarity;

import main.java.consolehelpers.Color;
import main.java.items.rarity.abstractions.Rarity;

public class Common implements Rarity {
    // Stat modifier
    private double powerModifier = 1;
    // Color for display purposes
    private String itemRarityColor = Color.WHITE;

    // Public properties
    public double powerModifier() {
        return powerModifier;
    }

    public String getItemRarityColor() {
        return itemRarityColor;
    }
}
