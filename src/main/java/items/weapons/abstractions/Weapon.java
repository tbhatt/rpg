package main.java.items.weapons.abstractions;

import main.java.items.rarity.*;
import main.java.items.rarity.abstractions.ItemRarity;

public interface Weapon {
    double getDamage();
    ItemRarity getRarity();
    //Used to test rarity
    Common common = new Common();
    Uncommon uncommon = new Uncommon();
    Rare rare = new Rare();
    Epic epic = new Epic();
    Legendary legendary = new Legendary();
}
