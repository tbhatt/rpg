package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.RangedWeapon;

public class Bow implements RangedWeapon {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.BOW_ATTACK_MOD;
    private double attackMultiplier;
    // Rarity
    private ItemRarity rarity;
    public Rarity rarityType;

    // Public properties
    public ItemRarity getRarity() {
        return rarity;
    }

    public double getDamage(){
        return this.attackPowerModifier * this.attackMultiplier;
    }

    // Constructors
    public Bow() {
        this.rarity = ItemRarity.Common;
        this.rarityType = common;
        this.attackMultiplier = common.powerModifier();
    }

    public Bow(Rarity rarity, ItemRarity itemRarity) {
        this.rarityType = rarity;
        this.rarity = itemRarity;
        this.attackMultiplier = rarity.powerModifier();
    }

}
