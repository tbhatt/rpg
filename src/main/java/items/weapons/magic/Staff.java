package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.MagicWeapon;

public class Staff implements MagicWeapon {
    // Stat modifiers
    private double magicPowerModifier = WeaponStatsModifiers.STAFF_MAGIC_MOD;
    private double attackMultiplier;
    // Rarity
    private ItemRarity rarity;
    public Rarity rarityType;

    // Public properties
    public ItemRarity getRarity() {
        return rarity;
    }

    public double getDamage(){
        return this.magicPowerModifier * this.attackMultiplier;
    }
    // Constructors
    public Staff() {
        this.rarity = ItemRarity.Common;
        this.rarityType = common;
        this.attackMultiplier = common.powerModifier();
    }

    public Staff(Rarity rarity, ItemRarity itemRarity) {
        this.rarityType = rarity;
        this.rarity = itemRarity;
        this.attackMultiplier = rarity.powerModifier();
    }
}
