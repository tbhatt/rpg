package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.MeleeWeapon;

public class Dagger implements MeleeWeapon {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.DAGGER_ATTACK_MOD;
    private double attackMultiplier;

    // Rarity
    private ItemRarity rarity;
    public Rarity rarityType;

    // Public properties
    public ItemRarity getRarity() {
        return rarity;
    }
    public double getDamage(){return this.attackPowerModifier * this.attackMultiplier;}

    // Constructors
    public Dagger() {
        this.rarity = ItemRarity.Common;
        this.rarityType = common;
        this.attackMultiplier = common.powerModifier();
    }

    public Dagger(Rarity rarity,ItemRarity itemRarity) {
        this.rarityType = rarity;
        this.rarity = itemRarity;
        this.attackMultiplier = rarity.powerModifier();
    }
}
