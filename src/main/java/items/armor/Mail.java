package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;
import main.java.items.rarity.abstractions.Rarity;

public class Mail implements Armor {
    // Stat modifiers
    private double healthModifier = ArmorStatsModifiers.MAIL_HEALTH_MODIFIER;
    private double physRedModifier = ArmorStatsModifiers.MAIL_PHYS_RED_MODIFIER;
    private double magicRedModifier = ArmorStatsModifiers.MAIL_MAGIC_RED_MODIFIER;
    // Rarity
    //private final double rarityModifier;
    public Rarity rarity;

    // Public properties
    public double getHealthModifier() {
        return healthModifier * rarity.powerModifier();
    }

    public double getPhysRedModifier() {
        return physRedModifier * rarity.powerModifier();
    }

    public double getMagicRedModifier() {
        return magicRedModifier * rarity.powerModifier();
    }

    public double getRarityModifier() {
        return rarity.powerModifier();
    }

    // Constructors
    public Mail(Rarity itemRarity) {
        this.rarity = itemRarity;
    }
}
