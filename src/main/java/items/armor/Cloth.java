package main.java.items.armor;
// Imports

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;
import main.java.items.rarity.abstractions.Rarity;

public class Cloth implements Armor {
    // Stat modifiers
    private double healthModifier = ArmorStatsModifiers.CLOTH_HEALTH_MODIFIER;
    private double physRedModifier = ArmorStatsModifiers.CLOTH_PHYS_RED_MODIFIER;
    private double magicRedModifier = ArmorStatsModifiers.CLOTH_MAGIC_RED_MODIFIER;
    // Rarity
    //private final double rarityModifier;
    public Rarity rarity;

    // Public properties
    public double getHealthModifier() { return healthModifier * rarity.powerModifier(); }

    public double getPhysRedModifier() {
        return physRedModifier * rarity.powerModifier();
    }

    public double getMagicRedModifier() {
        return magicRedModifier * rarity.powerModifier();
    }

    public double getRarityModifier() {
        return rarity.powerModifier();
    }

    // Constructors
    public Cloth(Rarity itemRarity) {
        this.rarity = itemRarity;
    }

}
