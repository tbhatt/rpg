package main.java.items.armor.abstractions;

public interface Armor {
    double getHealthModifier();

    double getPhysRedModifier();

    double getMagicRedModifier();

    double getRarityModifier();
}
