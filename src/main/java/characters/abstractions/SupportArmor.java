package main.java.characters.abstractions;

public interface SupportArmor {
    double shieldPartyMember(double partyMemberMaxHealth, Hero partyMember);

}
