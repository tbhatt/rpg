package main.java.characters.abstractions;

import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;

public interface Support extends Hero{
    // Metadata
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Support;
    public final ArmorType ARMOR_TYPE = ArmorType.Leather;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Magic;

    double healPartyMember(Hero partyMember);

}
