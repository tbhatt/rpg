package main.java.characters.abstractions;

import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;

public interface Caster extends Hero {
    CharacterCategory CHARACTER_CAT = CharacterCategory.Caster;
    ArmorType ARMOR_TYPE = ArmorType.Cloth;
    WeaponCategory WEAPON_TYPE = WeaponCategory.Magic;


}
