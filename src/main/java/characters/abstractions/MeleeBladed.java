package main.java.characters.abstractions;

public interface MeleeBladed extends Melee {
    double attackWithBladedWeapon();
}
