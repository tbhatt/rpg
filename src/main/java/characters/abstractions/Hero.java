package main.java.characters.abstractions;

import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.Weapon;

public interface Hero {
    double getCurrentHealth();
    double getCurrentMaxHealth();
    void equipArmor(Armor armor);
    void equipWeapon(Weapon weapon);

    void equipShield(double value);
    void heal(double heal);

    Boolean getDead();
}