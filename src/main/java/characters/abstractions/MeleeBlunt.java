package main.java.characters.abstractions;

public interface MeleeBlunt extends Melee{
    double attackWithBluntWeapon();

}
