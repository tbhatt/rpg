package main.java.characters.abstractions;

import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;

public interface Ranged extends Hero{
    // Metadata
    CharacterCategory CHARACTER_CAT = CharacterCategory.Ranged;
    ArmorType ARMOR_TYPE = ArmorType.Mail;
    WeaponCategory WEAPON_TYPE = WeaponCategory.Ranged;
}
