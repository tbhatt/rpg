package main.java.characters;
// Imports

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Ranged;
import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.ranged.Bow;

/*
 Rangers
 Are masters of ranged combat. They use a wide arsenal of weapons to dispatch enemies.
*/
public class Ranger implements Ranged {

    // Base stats defensive
    private double maxHealth;
    private double baseHealth = CharacterBaseStatsDefensive.RANGER_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.RANGER_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.RANGER_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseAttackPower = CharacterBaseStatsOffensive.RANGER_RANGED_ATTACK_POWER;
    //Weapon and armor;
    public Weapon weapon;
    public Armor armor;
    public double shield = 0;

    // Active trackers and flags
    private double currentHealth;
    private Boolean isDead = false;

    // Constructor
    public Ranger() {
        // When the character is created it has maximum health (base health)
        this.currentHealth = baseHealth;
        System.out.println("Created a " + CHARACTER_CAT);
        this.weapon = new Bow();

    }
    public void equipShield(double value){
        shield += value;
    }

    public void heal(double heal){
        if(currentHealth + heal > maxHealth) currentHealth = maxHealth;
        else currentHealth += heal;
    }
    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return maxHealth;
    }  // Needs alteration after equipment is in

    public double getCurrentHealth() {
        return currentHealth;
    }

    public Boolean getDead() {
        return isDead;
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Armor armor) {
        maxHealth = baseHealth * armor.getHealthModifier();
        this.armor = armor;
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    // Character behaviours

    /**
     * Damages the enemy
     */
    public double attackWithRangedWeapon() {
        return baseAttackPower * this.weapon.getDamage();
    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, String damageType) {
        Double damageTaken = 0.0;
        //Damage is reduced by armor
        if(damageType.equals("Physical")){
            damageTaken = incomingDamage * (1 - (basePhysReductionPercent * armor.getPhysRedModifier()));
        }
        else{
            damageTaken = incomingDamage * (1 - (baseMagicReductionPercent * armor.getMagicRedModifier()));
        }
        //If damage is < 1 we simplify to take 1 damage
        if (damageTaken<1) {damageTaken = 1.0;}

        //Reduce health/shield
        double remDamage = damageTaken;
        if(shield > 0){ //Remove shield if we have
            if(shield > remDamage) { //When shield > damage, we only lose shield
                shield -= remDamage;
                System.out.println("Remaining shield: " + shield);
                remDamage = 0;
            }
            else{  //When damage > shield we lose all shield
                remDamage -= shield;
                shield = 0;
                System.out.println("Shield broken by " + remDamage + " damage");
            }
        }
        currentHealth -= remDamage; //Remove health after shield and reduction
        if(currentHealth <= 0){ //Character dies
            currentHealth = 0;
            isDead = true;
        }
        return damageTaken ; // Return damage taken after damage reductions, based on type of damage.
    }
}
