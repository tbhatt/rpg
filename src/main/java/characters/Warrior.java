package main.java.characters;
// Imports

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.MeleeBladed;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.melee.Sword;

/*
 Warriors are combat veterans, durable forces on the battlefield.
 They are masters of the blade and wield it with unmatched ferocity.
*/
public class Warrior implements MeleeBladed {
    // Metadata
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Melee;
    public final ArmorType ARMOR_TYPE = ArmorType.Plate;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.BluntWeapon;

    // Base stats defensive
    private double maxHealth;
    private double baseHealth = CharacterBaseStatsDefensive.WARRIOR_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.WARRIOR_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.WARRIOR_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseAttackPower = CharacterBaseStatsOffensive.WARRIOR_MELEE_ATTACK_POWER;
    public Weapon weapon;
    public Armor armor;
    double shield = 0;

    // Active trackers and flags
    private double currentHealth;
    private Boolean isDead = false;

    // Constructor
    public Warrior() {
        // When the character is created it has maximum health (base health)
        this.currentHealth = baseHealth;
        System.out.println("Created a " + CHARACTER_CAT);
        this.weapon = new Sword();    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return maxHealth;
    }  // Needs alteration after equipment is in

    public double getCurrentHealth() {
        return currentHealth;
    }

    public Boolean getDead() {
        return isDead;
    }

    public void equipShield(double value){
        shield += value;
    }

    public void heal(double heal){
        if(currentHealth + heal > maxHealth) currentHealth = maxHealth;
        else currentHealth += heal;
    }
    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Armor armor) {
        maxHealth = baseHealth * armor.getHealthModifier();
        currentHealth = maxHealth;this.armor = armor;
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    // Character behaviours

    /**
     * Damages the enemy
     */
    public double attackWithBladedWeapon() {
        return baseAttackPower * this.weapon.getDamage(); // Replaced with actual damage amount based on calculations
    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, String damageType) {
        Double damageTaken = 0.0;
        if(damageType.equals("Physical")){
            damageTaken = incomingDamage * (1 - (basePhysReductionPercent * armor.getPhysRedModifier()));
        }
        else{
            damageTaken = incomingDamage * (1 - (baseMagicReductionPercent * armor.getMagicRedModifier()));
        }
        //If damage is < 1 we simplify to 1 damage
        if (damageTaken<1) {damageTaken = 1.0;}

        //Reduce health/shield
        double remDamage = damageTaken;
        if(shield > 0){
            if(shield > remDamage) {
                shield -= remDamage;
                System.out.println("Remaining shield: " + shield);
                remDamage = 0;
            }
            else{
                remDamage -= shield;
                shield = 0;
                System.out.println("Shield broken by " + remDamage + " damage");
            }
        }
        currentHealth -= remDamage;
        if(currentHealth <= 0){
            currentHealth = 0; //For reviving
            isDead = true;
        }
        return damageTaken ; // Return damage taken after damage reductions, based on type of damage.
    }
}
