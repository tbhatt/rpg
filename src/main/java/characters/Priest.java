package main.java.characters;
// Imports

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.Hero;
import main.java.characters.abstractions.Support;
import main.java.characters.abstractions.SupportArmor;
import main.java.factories.SpellFactory;
import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.Weapon;
import main.java.spells.abstractions.HealingSpell;
import main.java.spells.abstractions.ShieldingSpell;
import main.java.spells.abstractions.SpellType;

/*
 Priest are the servants of the light and goodness.
 They use holy magic to heal and shield allies.
 As a support class they only have defensive stats.
*/
public class Priest implements Support, SupportArmor {
    // Metadata
    private ShieldingSpell shieldingSpell;
    private HealingSpell healingSpell;

    // Base stats defensive
    private double maxHealth;
    private double baseHealth = CharacterBaseStatsDefensive.PRIEST_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.PRIEST_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.PRIEST_BASE_MAGIC_RES; // Magic armor

    public Weapon weapon;
    public Armor armor;
    double shield = 0;
    // Active trackers and flags
    private double currentHealth;
    private Boolean isDead = false;

    // Constructor
    public Priest() {
        // When the character is created it has maximum health (base health)
        this.currentHealth = baseHealth;
        this.healingSpell = (HealingSpell) SpellFactory.getCharacter(SpellType.Regrowth);
        this.shieldingSpell = (ShieldingSpell) SpellFactory.getCharacter(SpellType.Barrier);
    }

    public Priest(HealingSpell healingSpell, ShieldingSpell shieldingSpell) {
        this.currentHealth = baseHealth;
        this.healingSpell = healingSpell;
        this.shieldingSpell = shieldingSpell;
    }

    public void equipShield(double value){
        shield += value;
    }

    public void heal(double heal){
        if(currentHealth + heal > maxHealth) currentHealth = maxHealth;
        else currentHealth += heal;
    }
    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return maxHealth;
    }  // Needs alteration after equipment is in

    public double getCurrentHealth() {
        return currentHealth;
    }

    public Boolean getDead() {
        return isDead;
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Armor armor) {
        maxHealth = baseHealth * armor.getHealthModifier();
        this.armor = armor;
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    // Character behaviours

    /**
     * Heals the party member, increasing their current health.
     * @param partyMember
     */
    public double healPartyMember(Hero partyMember) {
        double healValue = healingSpell.getHealingAmount() * weapon.getDamage();
        partyMember.heal(healValue);
        return healValue;
    }

    /**
     * Shields a party member for a percentage of their maximum health.
     * @param partyMemberMaxHealth
     */
    public double shieldPartyMember(double partyMemberMaxHealth, Hero partyMember) {
        Double shieldValue = partyMemberMaxHealth * shieldingSpell.getAbsorbShieldPercentage() + weapon.getDamage();
        partyMember.equipShield(shieldValue);
        return shieldValue; // Return calculated shield value
    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, String damageType) {
        Double damageTaken = 0.0;
        if(damageType.equals("Physical")){
            damageTaken = incomingDamage * (1 - (basePhysReductionPercent * armor.getPhysRedModifier()));
        }
        else{
            damageTaken = incomingDamage * (1 - (baseMagicReductionPercent * armor.getMagicRedModifier()));
        }
        //If damage is < 1 we simplify to 1 damage
        if (damageTaken<1) {damageTaken = 1.0;}

        //Reduce health/shield
        double remDamage = damageTaken;
        if(shield > 0){
            if(shield > remDamage) {
                shield -= remDamage;
                System.out.println("Remaining shield: " + shield);
                remDamage = 0;
            }
            else{
                remDamage -= shield;
                shield = 0;
                System.out.println("Shield broken by " + remDamage + " damage");
            }
        }
        currentHealth -= remDamage;
        if(currentHealth <= 0){
            currentHealth = 0; //For reviving
            isDead = true;
        }
        return damageTaken ; // Return damage taken after damage reductions, based on type of damage.
    }
}
