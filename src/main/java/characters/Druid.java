package main.java.characters;
// Imports

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.Hero;
import main.java.characters.abstractions.Support;
import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.Weapon;
import main.java.spells.abstractions.HealingSpell;

/*
 Class description:
 ------------------
 Druids are spell casters who use nature based magic to aid their allies in battle.
 They can heal their allies or protect them using the forces of nature.
 As a support class they only have defensive stats.
*/
public class Druid implements Support {
    // Metadata
    private HealingSpell healingSpell;

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.DRUID_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.DRUID_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.DRUID_BASE_MAGIC_RES; // Magic armor
    private double maxHealth;

    public Weapon weapon;
    public Armor armor;
    public double shield=0;
    // Active trackers and flags
    private double currentHealth;
    private Boolean isDead = false;

    // Constructor
    public Druid() {
        // When the character is created it has maximum health (base health)
        this.currentHealth = baseHealth ;
        System.out.println("Created a " + CHARACTER_CAT);
    }

    public Druid(HealingSpell healingSpell) {
        this.currentHealth = baseHealth;
        this.healingSpell = healingSpell;
    }

    public void equipShield(double value){
        shield += value;
    }

    public void heal(double heal){
        if(currentHealth + heal > maxHealth) currentHealth = maxHealth;
        else currentHealth += heal;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return maxHealth;
    }  // Needs alteration after equipment is in

    public double getCurrentHealth() {
        return currentHealth;
    }

    public Boolean getDead() {
        return isDead;
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Armor armor) {
        this.armor = armor;
        maxHealth = baseHealth * armor.getHealthModifier();
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    // Character behaviours

    /**
     * Heals the party member
     */
    public double healPartyMember(Hero partyMember) {
        double healValue = healingSpell.getHealingAmount() * weapon.getDamage();
        partyMember.heal(healValue);
        return healValue;
    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, String damageType) {
        Double damageTaken = 0.0;
        if(damageType.equals("Physical")){
            damageTaken = incomingDamage * (1 - (basePhysReductionPercent * armor.getPhysRedModifier()));
        }
        else{
            damageTaken = incomingDamage * (1 - (baseMagicReductionPercent * armor.getMagicRedModifier()));
        }
        //If damage is < 1 we simplify to 1 damage
        if (damageTaken<1) {damageTaken = 1.0;}

        //Reduce health/shield
        double remDamage = damageTaken;
        if(shield > 0){
            if(shield > remDamage) {
                shield -= remDamage;
                System.out.println("Remaining shield: " + shield);
                remDamage = 0;
            }
            else{
                remDamage -= shield;
                shield = 0;
                System.out.println("Shield broken by " + remDamage + " damage");
            }
        }
        currentHealth -= remDamage;
        if(currentHealth <= 0){
            currentHealth = 0; //For reviving
            isDead = true;
        }
        return damageTaken ; // Return damage taken after damage reductions, based on type of damage.
    }
}
