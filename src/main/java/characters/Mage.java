package main.java.characters;
// Imports

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Caster;
import main.java.factories.SpellFactory;
import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.Weapon;
import main.java.spells.abstractions.DamagingSpell;
import main.java.spells.abstractions.Spell;
import main.java.spells.abstractions.SpellType;

/*
 Class description:
 ------------------
 Mages are masters of arcane magic. Their skills are honed after years of dedicated study.
 They conjure arcane energy to deal large amounts of damage to enemies.
 They are vulnerable to physical attacks but are resistant to magic.
*/
public class Mage implements Caster {
    // Spell
    private DamagingSpell damagingSpell;

    // Base stats defensive
    private double maxHealth;
    private final double baseHealth = CharacterBaseStatsDefensive.MAGE_BASE_HEALTH;
    private final double basePhysReductionPercent = CharacterBaseStatsDefensive.MAGE_BASE_PHYS_RED; // Armor
    private final double baseMagicReductionPercent = CharacterBaseStatsDefensive.MAGE_BASE_MAGIC_RES; // Magic armor

    // Base stats offensive
    private double baseMagicPower = CharacterBaseStatsOffensive.MAGE_MAGIC_POWER;

    public Weapon weapon;
    public Armor armor;
    double shield=0;

    // Active trackers and flags
    private double currentHealth;
    private Boolean isDead = false;

    // Constructor
    public Mage() {
        // When the character is created it has maximum health (base health)
        this.currentHealth = baseHealth;
        this.damagingSpell = (DamagingSpell) SpellFactory.getCharacter(SpellType.ArcaneMissile);
        System.out.println("Created a " + CHARACTER_CAT);

    }

    public Mage(Spell damagingSpell) {
        this.currentHealth = baseHealth;
        this.damagingSpell = (DamagingSpell) damagingSpell;
    }

    public void equipShield(double value){
        shield += value;
    }

    public void heal(double heal){
        if(currentHealth + heal > maxHealth) currentHealth = maxHealth;
        else currentHealth += heal;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return maxHealth;
    }  // Needs alteration after equipment is in

    public double getCurrentHealth() {
        return currentHealth;
    }

    public Boolean getDead() {
        return isDead;
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Armor armor) {
        maxHealth = baseHealth * armor.getHealthModifier();
        this.armor = armor;
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    // Character behaviours

    /**
     * Damages the enemy with its spells
     */
    public double castDamagingSpell() {
        return baseMagicPower * damagingSpell.getSpellDamageModifier() * weapon.getDamage(); // Replaced with actual damage amount based on calculations
    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, String damageType) {
        Double damageTaken = 0.0;
        if(damageType.equals("Physical")){
            damageTaken = incomingDamage * (1 - (basePhysReductionPercent * armor.getPhysRedModifier()));
        }
        else{
            damageTaken = incomingDamage * (1 - (baseMagicReductionPercent * armor.getMagicRedModifier()));
        }
        //If damage is < 1 we simplify to 1 damage
        if (damageTaken<1) {damageTaken = 1.0;}

        //Reduce health/shield
        double remDamage = damageTaken;
        if(shield > 0){
            if(shield > remDamage) {
                shield -= remDamage;
                System.out.println("Remaining shield: " + shield);
                remDamage = 0;
            }
            else{
                remDamage -= shield;
                shield = 0;
                System.out.println("Shield broken by " + remDamage + " damage");
            }
        }
        currentHealth -= remDamage;
        if(currentHealth <= 0){
            currentHealth = 0; //For reviving
            isDead = true;
        }
        return damageTaken ; // Return damage taken after damage reductions, based on type of damage.
    }
}
