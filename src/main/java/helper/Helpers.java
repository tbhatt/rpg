package main.java.helper;

import main.java.items.rarity.*;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;

public class Helpers {
    //Used to get rarity class from ItemRarity
    public static Rarity getRarity(ItemRarity newRarity){
        //System.out.println("Setting rarity to " + newRarity);
        if(newRarity == ItemRarity.Common){
            return new Common();
        }
        else if(newRarity == ItemRarity.Uncommon) {
            return new Uncommon();
        }
        else if(newRarity == ItemRarity.Rare){
            return new Rare();
        }
        else if(newRarity == ItemRarity.Epic) {
            return new Epic();
        }
        else if(newRarity == ItemRarity.Legendary){
            return new Legendary();
        }
        return null;
    }

}
