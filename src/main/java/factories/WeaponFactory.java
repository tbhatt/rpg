package main.java.factories;
// Imports
import main.java.helper.Helpers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.items.weapons.magic.Staff;
import main.java.items.weapons.magic.Wand;
import main.java.items.weapons.melee.*;
import main.java.items.weapons.ranged.Bow;
import main.java.items.weapons.ranged.Crossbow;
import main.java.items.weapons.ranged.Gun;
/*
 This factory exists to be responsible for creating new enemies.
 Object is replaced with Weapon as a return type when refactored to be good OO design.
*/
public class WeaponFactory {
    public static Weapon getItem(WeaponType weaponType, ItemRarity itemRarityModifier) {
        Rarity rarity = Helpers.getRarity(itemRarityModifier);
        switch(weaponType) {
            case Axe:
                return new Axe(rarity, itemRarityModifier);
            case Bow:
                return new Bow(rarity, itemRarityModifier);
            case Crossbow:
                return new Crossbow(rarity, itemRarityModifier);
            case Dagger:
                return new Dagger(rarity, itemRarityModifier);
            case Gun:
                return new Gun(rarity, itemRarityModifier);
            case Hammer:
                return new Hammer(rarity, itemRarityModifier);
            case Mace:
                return new Mace(rarity, itemRarityModifier);
            case Staff:
                return new Staff(rarity, itemRarityModifier);
            case Sword:
                return new Sword(rarity, itemRarityModifier);
            case Wand:
                return new Wand(rarity, itemRarityModifier);
            default:
                return null;
        }
    }
}
