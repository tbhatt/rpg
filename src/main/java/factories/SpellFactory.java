package main.java.factories;

import main.java.spells.abstractions.Spell;
import main.java.spells.abstractions.SpellType;
import main.java.spells.damaging.ArcaneMissile;
import main.java.spells.damaging.ChaosBolt;
import main.java.spells.healing.Regrowth;
import main.java.spells.healing.SwiftMend;
import main.java.spells.shielding.Barrier;
import main.java.spells.shielding.Rapture;

public class SpellFactory {
    public static Spell getCharacter(SpellType spellType) {
        Spell newSpell = null;
        switch(spellType) {
            case Barrier:
                newSpell = new Barrier();
                break;
            case Rapture:
                newSpell = new Rapture();
                break;
            case Regrowth:
                newSpell = new Regrowth();
                break;
            case ChaosBolt:
                newSpell = new ChaosBolt();
                break;
            case SwiftMend:
                newSpell = new SwiftMend();
                break;
            case ArcaneMissile:
                newSpell = new ArcaneMissile();
                break;
        }
        return newSpell;
    }
}
