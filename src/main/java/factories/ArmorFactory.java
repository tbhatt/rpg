package main.java.factories;
// Imports
import main.java.helper.Helpers;
import main.java.items.armor.Cloth;
import main.java.items.armor.Leather;
import main.java.items.armor.Mail;
import main.java.items.armor.Plate;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;

/*
 This factory exists to be responsible for creating new Armor.
 Object is replaced with some Item abstraction.
*/
public class ArmorFactory {
    public static Armor getArmor(ArmorType armorType, ItemRarity itemRarityModifier){
        Rarity rarity = Helpers.getRarity(itemRarityModifier);
        switch(armorType) {
            case Cloth:
                return new Cloth(rarity);
            case Leather:
                return new Leather(rarity);
            case Mail:
                return new Mail(rarity);
            case Plate:
                return new Plate(rarity);
            default:
                return null;
        }
    }
}
