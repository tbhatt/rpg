package main.java.factories;
// Imports

import main.java.characters.*;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.abstractions.Hero;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.WeaponType;

/*
 This factory exists to be responsible for creating new enemies.
 Object is replaced with Character as a return type when refactored to be good OO design.
*/
// TODO Once characters can be made with the right compositions, then implement the factory
public class CharacterFactory {
    public static Hero getCharacter(CharacterType characterType) {
        Hero hero = null;
        switch(characterType) {
            case Ranger:
                hero = new Ranger();
                hero.equipArmor(ArmorFactory.getArmor(ArmorType.Mail, ItemRarity.Common));
                hero.equipWeapon(WeaponFactory.getItem(WeaponType.Bow, ItemRarity.Common));
                break;
            case Mage:
                hero = new Mage();
                hero.equipArmor(ArmorFactory.getArmor(ArmorType.Cloth, ItemRarity.Common));
                hero.equipWeapon(WeaponFactory.getItem(WeaponType.Wand, ItemRarity.Common));
                break;
            case Druid:
                hero = new Druid();
                hero.equipArmor(ArmorFactory.getArmor(ArmorType.Leather, ItemRarity.Common));
                hero.equipWeapon(WeaponFactory.getItem(WeaponType.Staff, ItemRarity.Common));
                break;
            case Rogue:
                hero = new Rogue();
                hero.equipArmor(ArmorFactory.getArmor(ArmorType.Leather, ItemRarity.Common));
                hero.equipWeapon(WeaponFactory.getItem(WeaponType.Sword, ItemRarity.Common));
                break;
            case Priest:
                hero = new Priest();
                hero.equipArmor(ArmorFactory.getArmor(ArmorType.Cloth, ItemRarity.Common));
                hero.equipWeapon(WeaponFactory.getItem(WeaponType.Staff, ItemRarity.Common));
                break;
            case Paladin:
                hero = new Paladin();
                hero.equipArmor(ArmorFactory.getArmor(ArmorType.Plate, ItemRarity.Common));
                hero.equipWeapon(WeaponFactory.getItem(WeaponType.Hammer, ItemRarity.Common));
                break;
            case Warlock:
                hero = new Warlock();
                hero.equipArmor(ArmorFactory.getArmor(ArmorType.Cloth, ItemRarity.Common));
                hero.equipWeapon(WeaponFactory.getItem(WeaponType.Wand, ItemRarity.Common));
                break;
            case Warrior:
                hero = new Warrior();
                hero.equipArmor(ArmorFactory.getArmor(ArmorType.Plate, ItemRarity.Common));
                hero.equipWeapon(WeaponFactory.getItem(WeaponType.Axe, ItemRarity.Common));
                break;
        }
        System.out.println("Factory created " + hero.getClass().getName());
        return hero;
    }
}
