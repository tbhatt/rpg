package main.java;

import main.java.characters.Mage;
import main.java.characters.Paladin;
import main.java.characters.Priest;
import main.java.characters.Ranger;
import main.java.characters.abstractions.CharacterType;
import main.java.consolehelpers.Color;
import main.java.factories.ArmorFactory;
import main.java.factories.CharacterFactory;
import main.java.factories.WeaponFactory;
import main.java.gameloop.GameLogic;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

public class Main {
    public static void main(String[] args) {
        GameLogic gameLogic = new GameLogic();
        System.out.println(Color.RED + "RED COLORED" + Color.RESET + " NORMAL");
        //--CREATING HEROES--
        //Melee
        Paladin paladin = (Paladin) CharacterFactory.getCharacter(CharacterType.Paladin);
        //Ranged
        Ranger ranger = (Ranger) CharacterFactory.getCharacter(CharacterType.Ranger);
        //Caster
        Mage mage = (Mage) CharacterFactory.getCharacter(CharacterType.Mage);
        //Support
        Priest priest = (Priest) CharacterFactory.getCharacter(CharacterType.Priest);

        System.out.println(Color.GREEN + "\n--Damage testing--");
        System.out.println("Ranger attacked for " + ranger.attackWithRangedWeapon() + " damage");
        System.out.println("Paladin attacked for " + paladin.attackWithBluntWeapon() + " damage");
        System.out.println("Mage attacked for " + mage.castDamagingSpell() + " damage");

        System.out.println(Color.RED + "\n--Damage(100) taken testing--");
        System.out.println("Ranger took " + ranger.takeDamage(100, "Physical") + " damage");
        System.out.println("Paladin took " + paladin.takeDamage(100, "Physical") + " damage");
        System.out.println("Mage took " + mage.takeDamage(100, "Physical") + " damage");
        System.out.println("Priest took " + priest.takeDamage(100, "Physical") + " damage");

        System.out.println(Color.RESET + "\n--Health--");
        System.out.println("Ranger has " + ranger.getCurrentHealth() + "HP of " + ranger.getCurrentMaxHealth() + " HP");
        System.out.println("Paladin has " + paladin.getCurrentHealth() + "HP of " + paladin.getCurrentMaxHealth() + " HP");
        System.out.println("Mage has " + mage.getCurrentHealth() + "HP of " + mage.getCurrentMaxHealth() + " HP");
        System.out.println("Priest has " + priest.getCurrentHealth() + "HP of " + priest.getCurrentMaxHealth() + " HP");
        //Create legendary armor
        Armor cloth = ArmorFactory.getArmor(ArmorType.Cloth, ItemRarity.Legendary);
        Armor leather = ArmorFactory.getArmor(ArmorType.Leather, ItemRarity.Legendary);
        Armor mail = ArmorFactory.getArmor(ArmorType.Mail, ItemRarity.Legendary);
        Armor plate = ArmorFactory.getArmor(ArmorType.Plate, ItemRarity.Legendary);
        //Create legendary weapons
        Weapon staff = WeaponFactory.getItem(WeaponType.Staff, ItemRarity.Legendary);
        Weapon axe = WeaponFactory.getItem(WeaponType.Axe, ItemRarity.Legendary);
        Weapon crossbow = WeaponFactory.getItem(WeaponType.Gun, ItemRarity.Legendary);

        ranger.equipArmor(mail);
        ranger.equipWeapon(crossbow);
        paladin.equipArmor(plate);
        paladin.equipWeapon(axe);
        mage.equipArmor(cloth);
        mage.equipWeapon(staff);
        priest.equipArmor(cloth);
        priest.equipWeapon(staff);
        System.out.println(Color.YELLOW + "Everyone has LEGENDARY EQUIPMENT");

        System.out.println(Color.GREEN + "\n--Damage testing--");
        System.out.println("Ranger attacked for " + ranger.attackWithRangedWeapon() + " damage");
        System.out.println("Paladin attacked for " + paladin.attackWithBluntWeapon() + " damage");
        System.out.println("Mage attacked for " + mage.castDamagingSpell() + " damage");

        System.out.println(Color.RED + "\n--Damage(100) taken testing--");
        System.out.println("Ranger took " +ranger.takeDamage(100, "Magical") + " damage");
        System.out.println("Paladin took " + paladin.takeDamage(100, "Magical") + " damage");
        System.out.println("Mage took " + mage.takeDamage(100, "Magical") + " damage");
        System.out.println("Priest took " + priest.takeDamage(100, "Magical") + " damage");


        //Health
        System.out.println(Color.RESET + "\n--Health--");
        System.out.println("Ranger has " + ranger.getCurrentHealth() + "HP of " + ranger.getCurrentMaxHealth() + " HP");
        System.out.println("Paladin has " + paladin.getCurrentHealth() + "HP of " + paladin.getCurrentMaxHealth() + " HP");
        System.out.println("Mage has " + mage.getCurrentHealth() + "HP of " + mage.getCurrentMaxHealth() + " HP");
        System.out.println("Priest has " + priest.getCurrentHealth() + "HP of " + priest.getCurrentMaxHealth() + " HP");

        //Heal test
        System.out.println(Color.CYAN + "\nHealing test on RANGER");
        System.out.println("Ranger has " + ranger.getCurrentHealth() + "HP of " + ranger.getCurrentMaxHealth() + " HP");
        System.out.println("Priest heals Ranger for " + priest.healPartyMember(ranger));
        System.out.println("Ranger has " + ranger.getCurrentHealth() + "HP of " + ranger.getCurrentMaxHealth() + " HP");

        //Shield test
        double heroesMaxHealth = ranger.getCurrentMaxHealth() + paladin.getCurrentMaxHealth() +
                                 mage.getCurrentMaxHealth()   + priest.getCurrentMaxHealth();
        System.out.println("\nShield test on RANGER");
        System.out.println("Priest shields Ranger for " + priest.shieldPartyMember(heroesMaxHealth, ranger));

        System.out.println("\nRound 1(2 000 damage)");
        System.out.println("Ranger took " + ranger.takeDamage(3000, "Magical") + " damage");
        System.out.println("Ranger has " + ranger.getCurrentHealth() + "HP of " + ranger.getCurrentMaxHealth() + " HP");
        System.out.println("Ranger is dead:" + ranger.getDead());

        System.out.println("\nRound 2(7 000 damage to kill)");
        System.out.println("Ranger took " + ranger.takeDamage(7000, "Physical") + " damage");
        System.out.println("Ranger has " + ranger.getCurrentHealth() + "HP of " + ranger.getCurrentMaxHealth() + " HP");
        System.out.println("Ranger is dead: " + ranger.getDead());

        System.out.println("Program ended");
    }
}
