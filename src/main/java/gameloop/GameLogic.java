package main.java.gameloop;

import main.java.characters.abstractions.CharacterType;
import main.java.characters.abstractions.Hero;
import main.java.factories.CharacterFactory;

import java.util.Scanner;

public class GameLogic {
    Scanner sc = new Scanner(System.in);
    public void begin(){
        mainScreen();
    }
    /*
     The game logic goes as follows:
     1. Present main menu where the user can start a new game or see high scores (max floor reached and what party).
        e.g. Nick; Floor 50; Druid, Paladin, Rogue, Warrior.

     2. When the user selects new game, they are asked to create a party of 4 characters.
        They pick the 4 classes, the characters are generated and equipped with common items.
     3. After party creation they are presented with 3 options:
        - Easy encounter (normally scaled mobs)
        - Hard encounter (mobs scaled to be more difficult)
        - Town (A place to rest)
        3.1. If they pick town, the parties health is restored.
            TODO Implement gold cost for resting, and a shop to buy items.
     4. After an encounter type is picked, the encounter is generated.
        4.1. A group of enemies are randomly generated from the pool.
        4.2. These enemies are scaled based on the chosen difficulty for this floor and what floor they are on.
     5. The party then battles with the enemies generated for the floor.
        5.1. If the party is killed they user is taken to a game over screen.
            5.1.1. If they are in the top 10 runs, their name is asked and they are added to the leaderboard.
                    The leaderboard only contains the top 10 runs in order, so it needs to be updated.
     6. After the battle is over and the party has won, loot can drop.
        6.1. Loot is generated, better loot is given for harder difficulties and floor levels.
            TODO Implement a gold value of the items to sell them, this can be used to buy what the user wants in town.
     7. The floor number is incremented, and the user is presented with the options in step 3.
     8. The main game loop is steps 3-6.
    */

    public void mainScreen(){
        System.out.println("Welcome to the home screen");
        System.out.println("Enter 1 to start a new game");
        System.out.println("Enter 2 to check high scores");
        String command = getAlgorithm();
        Boolean run = true;
        while(run){
            System.out.println("You can only enter 1 or 2");
            if(command.equals("1") || command.equals("2")) {
                run = !run;
                break;
            }
            command = getAlgorithm();
        }

        CharacterType[] heroTypes = pickTeam(); //Select team
        Hero[] heroes = new Hero[4];
        for (int i = 0; i < heroes.length; i++) {
            heroes[i] = CharacterFactory.getCharacter(heroTypes[i]);
            System.out.println("Hero created: " + heroes[i]);
        }
    }
    public CharacterType[] pickTeam(){
        printCharacters();
        CharacterType[] team = new CharacterType[4];
        CharacterType type;
        for (int i = 0; i < 4; i++) {
            type = chooseCharacter();
            team[i] = type;
            if(type == null) i--;
        }
        return team;
    }

    public CharacterType chooseCharacter(){
        String character = getAlgorithm();

        for(CharacterType characterType: CharacterType.values() ){
            if (character.equalsIgnoreCase(characterType.name())){
                System.out.println("You chose " + characterType.name());
                return characterType;
            }
        }
        return null;
    }
    public String getAlgorithm(){
        System.out.print("Enter here: ");
        String input;
        if(sc.hasNext()){
            input = sc.nextLine();
            return input;
        }
        return null;
    }

    public void printCharacters(){
        System.out.println("Enter number to pick character");
        for (int i = 0; i < CharacterType.values().length; i++) {
            System.out.println(i+": " + CharacterType.values()[i]);
        }

    }

}
