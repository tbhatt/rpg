# RPG Tower Climb
* The goal of this project was to create a logical OOP design for Weapons, Armor, Characters.
* For character classes are mostly same. Check Ranger class for detailed explanation.
* I do have prints for shield in character classes to test better
* Started writing GameLogic, but its NOT ready
### Testing
* First i created a party of 4 heroes. One from each role. The factory created them with common Armor, Weapons and default spells. NB! Common armor will only increase max health and not the base health. Therefore, the heroes start with default HP as current health.
* Showed attack damage for all attacking heroes
* Every hero takes a hit and reduces it with armor and base stats
* Every hero shows its current health and max health. 
* Now legendary armor and weapons are created and equipped
* We don the same again with damage and taking hits
    * Damage increase
    * Damage resisted % increases
    * MaxHealth increases
* Now the healing/shielding is tested on Ranger by Priest since he has both functionalities
* For testing i made a helper class with functions:
    * Get a Rarity class from ItemRarity
